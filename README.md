[![pipeline status](https://gitlab.com/imtoori/converter/badges/master/pipeline.svg)](https://gitlab.com/imtoori/converter/commits/master)
[![coverage report](https://gitlab.com/imtoori/converter/badges/master/coverage.svg)](https://gitlab.com/imtoori/converter/commits/master)

# Converter

A simple Node app to convert money between different currencies at a given date.

Try it here https://sgconverter.herokuapp.com/convert?amount=1&src_currency=EUR&dest_currency=GBP&date=2019-11-05

## How to run

### Requirements

- Node.js (how to install: https://nodejs.org/it/download/)

### Commands

- clone repo
- `npm install`
- `cp .env.example .env`
- modify the env vars as you need

### Local development run

- `npm run dev`

### Production run

- `npm run build`
- `npm run start`

### Docker

- `docker run -p 8080:8080 salvatoreg/converter` 

### Testing

- `npm run test`

## CI/CD

On every push on every branch a testing pipeline is triggered.

On every push on master an additional deployment pipeline runs publishing a docker image (https://hub.docker.com/repository/docker/salvatoreg/converter) and a Heroku app (https://sgconverter.herokuapp.com/convert)

## Endpoint

There is only one edpoint available `GET /convert`

It accepts these query parameters:

```
    - amount: the amount to convert (e.g. 12.35)
    - src​_currency: ISO currency code for the source currency to convert (e.g. EUR, USD, GBP)
    - dest_currency: ISO currency code for the destination currency to convert (e.g. EUR, USD, GBP)
    - re​ference_date: reference date for the exchange rate, in YYYY-MM-DD format
```

### Success response

Code: 200

```
{
    "amount": 2,
    "currency": "USD"
}
```

### Error response

Code: 404

```
{
    message: "Date 2020-02-30 not found"
    code: "DateNotFound"
}
```

Code: 404

```
{
    message: "Currency KLM not found"
    code: "CurrencyNotFound"
}
```

Code: 400

```
{
    message: <ValidationMessage>
    code: "ValidationError"
}
```
