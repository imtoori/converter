export default class DateNotFoundError extends Error {
    public code = "DateNotFound";
    constructor(date: string) {
        super(`Date ${date} not found`);
        
        // https://stackoverflow.com/questions/41102060/typescript-extending-error-class
        // to reset prototype chain
        Object.setPrototypeOf(this, DateNotFoundError.prototype);
    }
}
