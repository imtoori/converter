import CurrencyNotFoundError from "../../src/errors/currencyNotFound.error";
import DateNotFoundError from "../../src/errors/dateNotFound.error";
import IRates from "../../src/models/rates";
import ConverterService from "../../src/services/converter.service";
import IConverterService from "../../src/services/iconverter.service";

describe("converter.service.ts", () => {
    let rates: IRates;
    let converterService: IConverterService;

    beforeAll(() => {
        rates = {
            "2019-11-12": {
                BGP: 2.34,
                USD: 1.23,
            }
        };
        converterService = new ConverterService(rates);
    });

    it("should convert correctly", () => {
        const amount = converterService.convertCurrency({
            date: "2019-11-12",
            destCurrency: "EUR",
            srcAmount: 1,
            srcCurrency: "BGP"
        });
        expect(amount).toBe<number>(0.43);
    });

    it("should throw a date not found error", () => {
        expect(() => {
            converterService.convertCurrency({
                date: "2019-11-10",
                destCurrency: "EUR",
                srcAmount: 1,
                srcCurrency: "BGP"
            });
        }).toThrowError(new DateNotFoundError("2019-11-10"));
    });

    it("should throw a currency not found error", () => {
        expect(() => {
            converterService.convertCurrency({
                date: "2019-11-12",
                destCurrency: "EUR",
                srcAmount: 1,
                srcCurrency: "BGD"
            });
        }).toThrowError(new CurrencyNotFoundError("BGD"));
    });
});
