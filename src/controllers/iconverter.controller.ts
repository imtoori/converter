import { Request, Response } from "express";

export interface IConverterController {
    get(req: Request, res: Response): void;
}
