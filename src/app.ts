import * as express from 'express';
import { Application } from 'express';
import { IConverterController } from 'controllers/iconverter.controller';
import { getValidators } from "./validators/converter.validators";
import { handleValidationResult } from "./middlewares/validation.middleware";
import { notFoundHandler } from "./middlewares/notFound.middleware";

class App {
    public app: Application;
    public port: number;

    constructor(options: { port: number; converterController: IConverterController; }) {
        this.app = express();
        this.port = options.port;

        this.app.get("/convert", getValidators, handleValidationResult, options.converterController.get);
        this.app.use(notFoundHandler);

    }

    public listen() {
        return this.app.listen(this.port);
    }
}

export default App;
