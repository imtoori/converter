import CurrencyNotFoundError from "../errors/currencyNotFound.error";
import DateNotFoundError from "../errors/dateNotFound.error";
import IRates from "../models/rates";
import IConverterService from "./iconverter.service";

export interface IConvertOptions {
    date: string;
    srcCurrency: string;
    destCurrency: string;
    srcAmount: number;
}

export default class ConverterService implements IConverterService {
    private rates: IRates;

    constructor(rates: IRates) {
        this.rates = rates;
    }

    public convertCurrency(options: IConvertOptions): number {
        const dailyRates = this.rates[options.date];
        if (!dailyRates) {
            throw new DateNotFoundError(options.date);
        }
        const srcRate = options.srcCurrency === "EUR" ? 1 : dailyRates[options.srcCurrency];
        if (!srcRate) {
            throw new CurrencyNotFoundError(options.srcCurrency);
        }
        const destRate = options.destCurrency === "EUR" ? 1 : dailyRates[options.destCurrency];
        if (!destRate) {
            throw new CurrencyNotFoundError(options.destCurrency);
        }
        const destAmount = (options.srcAmount / srcRate) * destRate;
        return Math.round(destAmount * 100 + Number.EPSILON) / 100;
    }
}
