import XmlParser from "../../src/services/xmlParser.service";
import { readFileSync } from "fs";

describe("xmlParser.service.ts", () => {
    let xml: string;
    beforeAll(() => {
        xml = readFileSync("./test/example.xml").toString();
    });

    it("should parse correctly", async () => {
        const xmlParser = new XmlParser();
        
        const rates = await xmlParser.parseXml(xml);

        expect(Object.keys(rates).length).toBe<number>(64);
        expect(rates["2019-12-23"]).toBeDefined();
        expect(Object.keys(rates["2019-12-23"]).length).toBe<number>(32);
        expect(rates["2019-12-23"].INR).toBeDefined();
    });

    it("should not parse correctly", async () => {
        const xmlParser = new XmlParser();

        // shuffle the xml file
        const corruptedXml = [...xml].sort(() => Math.random() - 0.5).join("");

        await expect(xmlParser.parseXml(corruptedXml)).rejects.toBeDefined();
    });
});
