import { query } from "express-validator";

export const getValidators = [
    query("amount", "amount should be a number").exists().isNumeric(),
    query("date").custom(value => {
        if (!/(^[12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))$/.test(value)) {
            throw new Error("date is required and should be a a date in the format YYYY-MM-DD");
        }
        return true;
    }),
    query("src_currency").custom(value => {
        if (!/^[A-Z]{3}$/g.test(value)) {
            throw new Error("src_currency is required and should be a valid currency");
        }
        return true;
    }),
    query("dest_currency").custom(value => {
        if (!/^[A-Z]{3}$/g.test(value)) {
            throw new Error("dest_currency is required and should be a valid currency");
        }
        return true;
    }),
];