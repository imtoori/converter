import { Request, Response } from "express";
import CurrencyNotFoundError from "../errors/currencyNotFound.error";
import DateNotFoundError from "../errors/dateNotFound.error";
import IConverterService from "../services/iconverter.service";
import { IConverterController } from "./iconverter.controller";

export default class ConverterController implements IConverterController {
    private converterService: IConverterService;
    constructor(converterService: IConverterService) {
        this.converterService = converterService;
    }

    public get = (req: Request, res: Response) => {
        const date: string = req.query.date;
        const srcAmount = Number(req.query.amount);
        const srcCurrency: string = req.query.src_currency;
        const destCurrency: string = req.query.dest_currency;

        try {
            const destAmount = this.converterService.convertCurrency({
                date,
                destCurrency,
                srcAmount,
                srcCurrency,
            });
            res.status(200).json({
                amount: destAmount,
                currency: destCurrency,
            });
        } catch (err) {
            if (err instanceof DateNotFoundError || err instanceof CurrencyNotFoundError) {
                res.status(404).json({
                    code: err.code,
                    message: err.message
                });
            } else {
                console.error("error handling request", err);
                res.status(500).end();
            }
        }

    }
}
