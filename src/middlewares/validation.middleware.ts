import { validationResult } from "express-validator";
import { Request, Response, NextFunction } from "express";

export function handleValidationResult(req: Request, res: Response, next: NextFunction) {
    const errors = validationResult(req);

    if (errors.isEmpty()) {
        return next();
    }

    const firstError = errors.array()[0];

    return res.status(400).json({ code: 'ValidationError', message: firstError.msg });

}
