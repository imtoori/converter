export interface IDailyRate {
    [currency: string]: number;
}
