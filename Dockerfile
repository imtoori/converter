FROM node:12-alpine

WORKDIR /usr/src/app

COPY .env.example .env.example

ENV PORT=8080
ENV XML_LINK=https://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist-90d.xml

COPY package*.json ./
RUN npm install

COPY src src
COPY tsconfig.json ./
RUN npm run build

EXPOSE 8080
CMD npm start
