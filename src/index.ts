import * as dotenvSafe from "dotenv-safe";
import ConverterController from "./controllers/converter.controller";
import ConverterService from "./services/converter.service";
import XmlParser from "./services/xmlParser.service";
import * as request from "request-promise-native";
import App from "./app";

dotenvSafe.config();

const xmlLink = process.env.XML_LINK;
const port = Number(process.env.PORT);

const startServer = async () => {
    const xml = await request.get(xmlLink);
    const xmlParser = new XmlParser();
    const rates = await xmlParser.parseXml(xml);

    const converterService = new ConverterService(rates);
    const converterController = new ConverterController(converterService);

    const app = new App({ port, converterController });

    app.listen();
};

startServer()
    .then(() => {
        console.log("Server started listening on port " + port);
    })
    .catch(error => {
        console.log("error running the server", error.toString());
    });
