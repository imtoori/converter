import IRates from "../models/rates";
import { Parser } from "xml2js";
import { promisify } from "util";
import IXmlParser from "./ixmlParser.service";

export default class XmlParser implements IXmlParser {
    // to use async/await
    private asyncParse: ({ (arg0: string): any; (arg1: string): Promise<object>; });

    constructor() {
        const parser = new Parser();
        this.asyncParse = promisify<string, object>(parser.parseString);
    }

    public async parseXml(xml: string): Promise<IRates> {
        const res = await this.asyncParse(xml);

        const days = res["gesmes:Envelope"].Cube[0].Cube;
        const dailyRates: IRates = {};
        days.forEach((day: any) => {
            const date = day.$.time;
            day.Cube.forEach((cube: any) => {
                const currency = cube.$.currency;
                const rate = Number(cube.$.rate);
                dailyRates[date] = {
                    ...dailyRates[date],
                    [currency]: rate
                };
            });
        });
        return dailyRates;
    }
}
