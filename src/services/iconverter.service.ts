import { IConvertOptions } from "../services/converter.service";

export default interface IConverterService {
    convertCurrency(options: IConvertOptions): number;
}
