import { IDailyRate } from "../models/dailyRate";

export default interface IRates {
    [date: string]: IDailyRate;
}
