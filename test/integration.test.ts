import * as request from "supertest";
import App from "../src/app";
import ConverterController from "../src/controllers/converter.controller";
import ConverterService from "../src/services/converter.service";
import XmlParser from "../src/services/xmlParser.service";
import { readFileSync } from "fs";

describe("intergration.test", () => {
    let app: App;

    beforeAll(async () => {
        const xml = readFileSync("./test/example.xml").toString();
        const xmlParser = new XmlParser();
        const rates = await xmlParser.parseXml(xml);
        const converterService = new ConverterService(rates);
        const converterController = new ConverterController(converterService);
        app = new App({ port: 8080, converterController: converterController });
    });

    it("should return the correct amount", async () => {
        const res = await request(app.app)
            .get("/convert")
            .query({
                amount: 10,
                date: "2019-11-12",
                dest_currency: "EUR",
                src_currency: "USD",
            });
        
        expect(res.status).toBe<number>(200);
        expect(res.body).toStrictEqual({
            amount: 9.08,
            currency: "EUR",
        });
    });

    it("should return 404", async () => {
        const res = await request(app.app)
            .get("/");
        
        expect(res.status).toBe<number>(404);
    });

    it("should return date not found", async () => {
        const res = await request(app.app)
            .get("/convert")
            .query({
                amount: 10,
                date: "2015-11-12",
                dest_currency: "EUR",
                src_currency: "USD",
            });
        
        expect(res.status).toBe<number>(404);
        expect(res.body).toStrictEqual({
            message: "Date 2015-11-12 not found",
            code: "DateNotFound",
        });
    });

    it("should return src currency not found", async () => {
        const res = await request(app.app)
            .get("/convert")
            .query({
                amount: 10,
                date: "2019-11-12",
                dest_currency: "EUR",
                src_currency: "USL",
            });
        
        expect(res.status).toBe<number>(404);
        expect(res.body).toStrictEqual({
            message: "Currency USL not found",
            code: "CurrencyNotFound",
        });
    });

    it("should return dest currency not found", async () => {
        const res = await request(app.app)
            .get("/convert")
            .query({
                amount: 10,
                date: "2019-11-12",
                dest_currency: "USL",
                src_currency: "EUR",
            });
        
        expect(res.status).toBe<number>(404);
        expect(res.body).toStrictEqual({
            message: "Currency USL not found",
            code: "CurrencyNotFound",
        });
    });

    it("should return bad amount validation error", async () => {
        const res = await request(app.app)
            .get("/convert")
            .query({
                amount: "test",
                date: "2019-11-12",
                dest_currency: "USD",
                src_currency: "EUR",
            });
        
        expect(res.status).toBe<number>(400);
        expect(res.body).toStrictEqual({
            message: "amount should be a number",
            code: "ValidationError",
        });
    });

    it("should return validation error for amount not existing", async () => {
        const res = await request(app.app)
            .get("/convert")
            .query({
                date: "2019-11-12",
                dest_currency: "USD",
                src_currency: "EUR",
            });
        
        expect(res.status).toBe<number>(400);
        expect(res.body).toStrictEqual({
            message: "amount should be a number",
            code: "ValidationError",
        });
    });

    it("should return bad date validation error", async () => {
        const res = await request(app.app)
            .get("/convert")
            .query({
                amount: 10,
                date: "2019-11-120",
                dest_currency: "USD",
                src_currency: "EUR",
            });
        
        expect(res.status).toBe<number>(400);
        expect(res.body).toStrictEqual({
            message: "date is required and should be a a date in the format YYYY-MM-DD",
            code: "ValidationError",
        });
    });

    it("should return validation error for date not existing", async () => {
        const res = await request(app.app)
            .get("/convert")
            .query({
                amount: 10,
                dest_currency: "USD",
                src_currency: "EUR",
            });
        
        expect(res.status).toBe<number>(400);
        expect(res.body).toStrictEqual({
            message: "date is required and should be a a date in the format YYYY-MM-DD",
            code: "ValidationError",
        });
    });

    it("should return bad currency validation error for src", async () => {
        const res = await request(app.app)
            .get("/convert")
            .query({
                amount: 10,
                date: "2019-11-12",
                dest_currency: "USD",
                src_currency: "EURa",
            });
        
        expect(res.status).toBe<number>(400);
        expect(res.body).toStrictEqual({
            message: "src_currency is required and should be a valid currency",
            code: "ValidationError",
        });
    });

    it("should return bad currency validation error for src not existing", async () => {
        const res = await request(app.app)
            .get("/convert")
            .query({
                amount: 10,
                date: "2019-11-12",
                dest_currency: "USD",
            });
        
        expect(res.status).toBe<number>(400);
        expect(res.body).toStrictEqual({
            message: "src_currency is required and should be a valid currency",
            code: "ValidationError",
        });
    });

    it("should return bad currency validation error for dest", async () => {
        const res = await request(app.app)
            .get("/convert")
            .query({
                amount: 10,
                date: "2019-11-12",
                dest_currency: "USDa",
                src_currency: "EUR",
            });
        
        expect(res.status).toBe<number>(400);
        expect(res.body).toStrictEqual({
            message: "dest_currency is required and should be a valid currency",
            code: "ValidationError",
        });
    });

    it("should return bad currency validation error for dest not existing", async () => {
        const res = await request(app.app)
            .get("/convert")
            .query({
                amount: 10,
                date: "2019-11-12",
                src_currency: "EUR",
            });
        
        expect(res.status).toBe<number>(400);
        expect(res.body).toStrictEqual({
            message: "dest_currency is required and should be a valid currency",
            code: "ValidationError",
        });
    });
});
