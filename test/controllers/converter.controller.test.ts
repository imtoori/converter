import { Request, Response } from "express";
import ConverterController from "../../src/controllers/converter.controller";
import CurrencyNotFoundError from "../../src/errors/currencyNotFound.error";
import DateNotFoundError from "../../src/errors/dateNotFound.error";
import IConverterService from "../../src/services/iconverter.service";

describe("converter.controller.ts", () => {
    let mockConverterService: jest.Mocked<IConverterService>;
    let converterController: ConverterController;

    beforeAll(() => {
        mockConverterService = {
            convertCurrency: jest.fn()
        };
        converterController = new ConverterController(mockConverterService);
    });

    beforeEach(() => {
        jest.clearAllMocks();
    });

    afterAll(() => {
        jest.restoreAllMocks();
    });

    it("should responde 200", () => {
        const mockReq = {
            query: {
                amount: 10,
                date: "2019-11-12",
                dest_currency: "EUR",
                src_currency: "USD",
            }
        } as unknown as jest.Mocked<Request>;
        const mockRes = {
            json: jest.fn(),
            status: jest.fn(),
        } as unknown as jest.Mocked<Response>;

        mockConverterService.convertCurrency.mockReturnValue(10);
        mockRes.status.mockReturnThis();

        converterController.get(mockReq, mockRes);

        expect(mockRes.status).toHaveBeenCalledWith(200);
        expect(mockRes.json).toHaveBeenCalledWith({
            amount: 10,
            currency: "EUR"
        });
    });

    it("should responde 404", () => {
        const mockReq = {
            query: {
                amount: 10,
                date: "2019-09-12",
                dest_currency: "EUR",
                src_currency: "USD",
            }
        } as unknown as jest.Mocked<Request>;
        const mockRes = {
            json: jest.fn(),
            status: jest.fn(),
        } as unknown as jest.Mocked<Response>;

        mockConverterService.convertCurrency.mockImplementation(() => {
            throw new DateNotFoundError(mockReq.query.date);
        });
        mockRes.status.mockReturnThis();

        converterController.get(mockReq, mockRes);
        expect(mockRes.status).toHaveBeenCalledWith(404);
        expect(mockRes.json).toHaveBeenCalledWith({
            code: "DateNotFound",
            message: `Date ${mockReq.query.date} not found`,
        });
    });

    it("should responde 404", () => {
        const mockReq = {
            query: {
                amount: 10,
                date: "2019-11-12",
                dest_currency: "EUR",
                src_currency: "USR",
            }
        } as unknown as jest.Mocked<Request>;
        const mockRes = {
            json: jest.fn(),
            status: jest.fn(),
        } as unknown as jest.Mocked<Response>;

        mockConverterService.convertCurrency.mockImplementation(() => {
            throw new CurrencyNotFoundError(mockReq.query.src_currency);
        });
        mockRes.status.mockReturnThis();

        converterController.get(mockReq, mockRes);
        expect(mockRes.status).toHaveBeenCalledWith(404);
        expect(mockRes.json).toHaveBeenCalledWith({
            code: "CurrencyNotFound",
            message: `Currency ${mockReq.query.src_currency} not found`,
        });
    });

    it("should responde 404", () => {
        const mockReq = {
            query: {
                amount: 10,
                date: "2019-11-12",
                dest_currency: "EUR",
                src_currency: "USR",
            }
        } as unknown as jest.Mocked<Request>;
        const mockRes = {
            json: jest.fn(),
            status: jest.fn(),
        } as unknown as jest.Mocked<Response>;

        mockConverterService.convertCurrency.mockImplementation(() => {
            throw new CurrencyNotFoundError(mockReq.query.dest_currency);
        });
        mockRes.status.mockReturnThis();

        converterController.get(mockReq, mockRes);
        expect(mockRes.status).toHaveBeenCalledWith(404);
        expect(mockRes.json).toHaveBeenCalledWith({
            code: "CurrencyNotFound",
            message: `Currency ${mockReq.query.dest_currency} not found`,
        });
    });

    it("should responde 500", () => {
        const mockReq = {
            query: {
                amount: 10,
                date: "2019-11-12",
                dest_currency: "EUR",
                src_currency: "USR",
            }
        } as unknown as jest.Mocked<Request>;
        const mockRes = {
            json: jest.fn(),
            status: jest.fn(),
            end: jest.fn(),
        } as unknown as jest.Mocked<Response>;

        mockConverterService.convertCurrency.mockImplementation(() => {
            throw new Error("test")
        });
        mockRes.status.mockReturnThis();

        converterController.get(mockReq, mockRes);
        expect(mockRes.status).toHaveBeenCalledWith(500);
    });

});
