export default class CurrencyNotFoundError extends Error {
    public code = "CurrencyNotFound";
    constructor(currency: string) {
        super(`Currency ${currency} not found`);

        // https://stackoverflow.com/questions/41102060/typescript-extending-error-class
        // to reset prototype chain
        Object.setPrototypeOf(this, CurrencyNotFoundError.prototype);
    }
}
