import IRates from "../models/rates";

export default interface IXmlParser {
    parseXml(xml: string): Promise<IRates>;
}